﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRMProject.DAL.Repositories
{
    interface IRepo<T>
    {
        T GetById(int Id);
        List<T> GetAll();

        int Add(T item);
        T Update(T item);
        void Remove(T item);
    }
}
