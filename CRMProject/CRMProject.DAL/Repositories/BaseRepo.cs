﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Reflection;
using Microsoft.EntityFrameworkCore;

namespace CRMProject.DAL.Repositories
{
    class BaseRepo<T> : IDisposable, IRepo<T> where T : class
    {
        private CRMContext db;

        public BaseRepo(DbContext crmCtx)
        {
            db = (CRMContext) crmCtx;
        }

        public int Add(T item)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            db.Dispose();
        }

        public List<T> GetAll()
        {
            throw new NotImplementedException();
        }

        public T GetById(int Id)
        {
            throw new NotImplementedException();
        }

        public void Remove(T item)
        {
            throw new NotImplementedException();
        }

        public T Update(T item)
        {
            throw new NotImplementedException();
        }
    }
}
