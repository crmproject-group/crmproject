﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRMProject.DAL
{
    class UnitOfWork : IDisposable
    {
        private CRMContext db;
        public void Dispose()
        {
            db.Dispose();
        }

        public void SaveChanges()
        {
            db.SaveChanges();
        }
    }
}
