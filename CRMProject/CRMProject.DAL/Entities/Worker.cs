﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRMProject.DAL.Entities
{
    class Worker : Person
    {
        public int Id { get; set; }
        public int PersonelId { get; set; }
        public string Tittle { get; set; }
        public string Country { get; set; }
        public DateTime HireDate { get; set; }
        public Nullable<DateTime> ReleaseDate { get; set; }
        public double Salary { get; set; }
        public double WorkingHour { get; set; }
    }
}
