﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRMProject.DAL.Entities
{
    class Product 
    {
        public int Id { get; set; }
        public string ProductName { get; set; }
        public double UnitPrice { get; set; }
        public double UnitStock { get; set; }
    }
}
