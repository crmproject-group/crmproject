﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRMProject.DAL.Entities
{
    class Task 
    {
        public int Id { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EstimatedFinishTime { get; set; }
        public Nullable<DateTime> FinishTime { get; set; }
        public bool Done { get; set; }
    }
}
