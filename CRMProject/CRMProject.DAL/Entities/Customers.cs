﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRMProject.DAL.Entities
{
    class Customers 
    {
        public int Id { get; set; }
        public string ContactName { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
    }
}
