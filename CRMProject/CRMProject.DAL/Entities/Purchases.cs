﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CRMProject.DAL.Entities
{
    class Purchases
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public double Quantity { get; set; }
        [Required]
        public double Price { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime DepartureDate { get; set; }
        public DateTime EstimatedTimeOfArrival { get; set; }
        public DateTime ArrivalTime { get; set; }
        public string TransportType { get; set; }
        public string Note { get; set; }

        [ForeignKey("Supplier")]
        [Required]
        public int SupplierId { get; set; }
        public string SupplierName { get; set; }
        public Supplier Supplier { get; set; }

        [ForeignKey("Product")]
        [Required]
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public Product Product { get; set; }
    }
}
