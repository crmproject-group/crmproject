﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRMProject.DAL.Entities
{
    class Person
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDay { get; set; }
        public string Email { get; set; }
    }
}
