﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRMProject.DAL.Entities
{
    class Meeting
    {
        public int Id { get; set; }
        public DateTime MeetingDate { get; set; }
        public string Subject { get; set; }
        public string Notes { get; set; }
    }
}
