﻿using CRMProject.BLL.Dtos;
using CRMProject.BLL.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRMProject.BLL.Business
{
    class OrderService : IOrderService<OrderDto>
    {
        public OrderDto CreateOrder(OrderDto item)
        {
            throw new NotImplementedException();
        }

        public IList<OrderDto> GetAllOrders()
        {
            throw new NotImplementedException();
        }

        public IList<OrderDto> GetByCustomerNameInOrders(string customerName)
        {
            throw new NotImplementedException();
        }

        public OrderDto GetByIdOrder(int id)
        {
            throw new NotImplementedException();
        }

        public IList<OrderDto> OrdersOnTheWay()
        {
            throw new NotImplementedException();
        }

        public IList<OrderDto> ReachedOrders()
        {
            throw new NotImplementedException();
        }

        public void Remove(OrderDto item)
        {
            throw new NotImplementedException();
        }

        public OrderDto UpdateOrder(OrderDto item)
        {
            throw new NotImplementedException();
        }
    }
}
