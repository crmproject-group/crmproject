﻿using CRMProject.BLL.Dtos;
using CRMProject.BLL.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRMProject.BLL.Business
{
    class WorkFollowService : ITaskService<TaskDto>
    {
        public TaskDto AddTask(TaskDto item)
        {
            throw new NotImplementedException();
        }

        public void DeleteTask()
        {
            throw new NotImplementedException();
        }

        public IList<TaskDto> GetAllTasks()
        {
            throw new NotImplementedException();
        }

        public TaskDto GetById(int id)
        {
            throw new NotImplementedException();
        }

        public IList<TaskDto> GetDoneTasks(bool done)
        {
            throw new NotImplementedException();
        }

        public TaskDto UpdateTask(TaskDto item)
        {
            throw new NotImplementedException();
        }
    }
}
