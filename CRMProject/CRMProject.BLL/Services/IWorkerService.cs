﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRMProject.BLL.Services
{
    interface IWorkerService<T>
    {
        T AddWorker(T item);
        T UpdateWorker(T item);
        void DeleteWorker();
        IList<T> GetAllWorker();
        T GetById(int id);
        T GetByPersoneId(int personelId);

    }
}
