﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRMProject.BLL.Services
{
    interface ITaskService<T>
    {
        T AddTask(T item);
        T UpdateTask(T item);
        void DeleteTask();
        IList<T> GetAllTasks();
        T GetById(int id);
        IList<T> GetDoneTasks(bool done);
    }
}
