﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRMProject.BLL.Services
{
    interface IProductService<T>
    {
        int CreateProduct(T item);
        T ChangeUnitPrice(T item);
        List<T> GetAllProduct();
        List<T> GetAllOnlyInStockProduct();
        double AddToProductStock(T item, double stock);
        void DeleteProduct(T item);
        bool IsTheProductInStock(T item);
        T GetById(int id);
    }
}
