﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRMProject.BLL.Services
{
    interface IInventoryService<T>
    {
        T CreatePurchases(T item);
        T UpdatePurchases(T item);
        IList<T> GetAllPurchases();
        IList<T> PurchasesOnTheWay();
        IList<T> ReachedPurchases();
        T GetByIdPurchases(int id);
        IList<T> GetByCustomerNameInPurchases(string supplierName);
        void Remove(T item);
    }
}
