﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRMProject.BLL.Services
{
    interface ICustomerService<T>
    {
        void Remove(T item);
        void Update(T item);
        T GetById(int Id);
        //IQueryable<T> FindBy(Expression<Func<T, bool>> predicate);
        T Add(T item);
        List<T> GetAll();
    }
}
