﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRMProject.BLL.Services
{
    interface IMeetingService<T>
    {
        T Create(T item);
        List<T> PlannedMeetings();

        List<T> DoneMeetings();

        List<T> OnComingMeetings();
    }
}
