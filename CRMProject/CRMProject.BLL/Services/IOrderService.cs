﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRMProject.BLL.Services
{
    interface IOrderService<T>
    {
        T CreateOrder(T item);
        T UpdateOrder(T item);
        IList<T> GetAllOrders();
        IList<T> OrdersOnTheWay();
        IList<T> ReachedOrders();
        T GetByIdOrder(int id);
        IList<T> GetByCustomerNameInOrders(string customerName);
        void Remove(T item);

    }
}
