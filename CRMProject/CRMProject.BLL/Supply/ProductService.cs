﻿using System;
using System.Collections.Generic;
using System.Text;
using CRMProject.BLL.Dtos;
using CRMProject.BLL.Services;

namespace CRMProject.BLL.Supply
{
    class ProductService : IProductService<ProductDto>
    {
        public double AddToProductStock(ProductDto item, double stock)
        {
            throw new NotImplementedException();
        }

        public ProductDto ChangeUnitPrice(ProductDto item)
        {
            throw new NotImplementedException();
        }

        public int CreateProduct(ProductDto item)
        {
            throw new NotImplementedException();
        }

        public void DeleteProduct(ProductDto item)
        {
            throw new NotImplementedException();
        }

        public List<ProductDto> GetAllOnlyInStockProduct()
        {
            throw new NotImplementedException();
        }

        public List<ProductDto> GetAllProduct()
        {
            throw new NotImplementedException();
        }

        public ProductDto GetById(int id)
        {
            throw new NotImplementedException();
        }

        public bool IsTheProductInStock(ProductDto item)
        {
            throw new NotImplementedException();
        }
    }
}
