﻿using CRMProject.BLL.Dtos;
using CRMProject.BLL.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRMProject.BLL.Supply
{
    class InventoryService : IInventoryService<PurchasesDto>
    {
        public PurchasesDto CreatePurchases(PurchasesDto item)
        {
            throw new NotImplementedException();
        }

        public IList<PurchasesDto> GetAllPurchases()
        {
            throw new NotImplementedException();
        }

        public IList<PurchasesDto> GetByCustomerNameInPurchases(string supplierName)
        {
            throw new NotImplementedException();
        }

        public PurchasesDto GetByIdPurchases(int id)
        {
            throw new NotImplementedException();
        }

        public IList<PurchasesDto> PurchasesOnTheWay()
        {
            throw new NotImplementedException();
        }

        public IList<PurchasesDto> ReachedPurchases()
        {
            throw new NotImplementedException();
        }

        public void Remove(PurchasesDto item)
        {
            throw new NotImplementedException();
        }

        public PurchasesDto UpdatePurchases(PurchasesDto item)
        {
            throw new NotImplementedException();
        }
    }
}
