﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRMProject.BLL.Dtos
{
    class TaskDto
    {
        public int Id { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EstimatedFinishTime { get; set; }
        public Nullable<DateTime> FinishTime { get; set; }
        public bool Done { get; set; }
    }
}
