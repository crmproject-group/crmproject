﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRMProject.BLL.Dtos
{
    class MeetingDto
    {
        public DateTime ToplantiTarihi { get; set; }
        public string Konu { get; set; }
        public string Notlar { get; set; }
    }
}
