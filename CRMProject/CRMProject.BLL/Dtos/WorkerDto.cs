﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRMProject.BLL.Dtos
{
    class WorkerDto : PersonDto
    {
        public int Id { get; set; }
        public string Tittle { get; set; }
        public string Country { get; set; }
        public DateTime HireDate { get; set; }
        public double Salary { get; set; }
        public double WorkingHour { get; set; }
    }
}