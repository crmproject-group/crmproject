﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRMProject.BLL.Dtos
{
    class OrderDto
    {
        public int Id { get; set; }
        public double Quantity { get; set; }
        public double Price { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime DepartureDate { get; set; }
        public DateTime EstimatedTimeOfArrival { get; set; }
        public DateTime ArrivalTime { get; set; }
        public string TransportType { get; set; }
        public string CustomerNote { get; set; }

        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
    }
}
